#include <stdlib.h>
#include <iostream>
#include <sstream>
#include <vector>
#include "TString.h"
#include "village_system.h"
//#include <limits>
#include "TSystem.h"
#include "TFile.h"
#include "TTree.h"
#include "configReader.h"



using namespace std;



int main(int argc, char* argv[]) {
  //do something
  double version = 2.3;
  TString confFileName = "";
  if (argc<2) {
    std::string helperString;
    std::cout<<"Enter a file name of a configuration file: ";
    getline(std::cin,helperString);
    confFileName = helperString;
  } else if (argc==2) {
    confFileName = TString(argv[1]);
  } else {
    throw std::runtime_error("Illegal number of arguments");
  }
  if (!confFileName.EndsWith(".cfg",TString::kIgnoreCase)) confFileName.Append(".cfg");
  configReader config(confFileName);
  
  TString fileName = config.getOutputFileName();
  int nRuns = config.getNRuns();
  
  
  long long nMaxTicks = config.getMaxTime()*60*60*20;

  std::cout<<"Starting simulation...."<<std::endl;
  TFile * outfile = TFile::Open(fileName.Data(),"UPDATE");
  TString treeName("");
  treeName +=  config.getTreeName(confFileName);
  std::cout<<"Tree name: "<<treeName<<std::endl;
  TObject * tempObj;
  TTree * tree;
  tempObj = outfile->Get(treeName.Data());
  if (tempObj && tempObj->InheritsFrom(TTree::Class())){
    std::cout<<"Found an existing TTree"<<std::endl;
    tree = (TTree*) tempObj;
    tempObj = NULL;
  } else {
    std::cout<<"Creating new TTree."<<std::endl;
    tree = new TTree(treeName.Data(),treeName.Data());
  }
  long long ticksBeforeFail;
  tree->Branch("TicksToFirstBreakdown",&ticksBeforeFail);
  tree->Branch("MaxTicks",&nMaxTicks);
  tree->Branch("SimulatorVersion",&version);
  int nVillagers = config.getNDetectors();
  int nSpectators = config.getNSpectators();
  int nPopulation = config.getNPopulation();
  int weightVillagers = config.getDetectorMultiplicity();
  int weightSpectators = config.getSpectatorMultiplicity();
  int weightPopulation = config.getPopulationMultiplicity();
  
  tree->Branch("nDetectorPositions",&nVillagers);
  tree->Branch("nSpectatorPositions",&nSpectators);
  tree->Branch("nPopulationPositions",&nPopulation);
  tree->Branch("detectorMultiplicity",&weightVillagers);
  tree->Branch("spectatorMultiplicity",&weightSpectators);
  tree->Branch("populationMultiplicity",&weightPopulation);
  
  for (int run=0;run<nRuns;run++) {
    std::cout<<"Simulating village behaviour: Run "<<run<<std::endl;
    village_system vsys(&config);
    if (run==0) vsys.print();
    ticksBeforeFail = 0;
    int status = vsys.tick();
    while(status==-1 && (ticksBeforeFail<nMaxTicks||nMaxTicks<0)) {
      ticksBeforeFail++;
      if (nMaxTicks>0 &&ticksBeforeFail%(nMaxTicks/10) == 0) std::cout<<"  Simulated "<<ticksBeforeFail<<" ticks ("<<(ticksBeforeFail/60./60./20.)<<" h)"<<std::endl;
      else if (nMaxTicks<0 && ticksBeforeFail%(60*60*20) == 0) std::cout<<"  Simulated "<<ticksBeforeFail<<" ticks ("<<(ticksBeforeFail/60./60./20.)<<" h)"<<std::endl;
      status = vsys.tick();
    }
    if (ticksBeforeFail == nMaxTicks && nMaxTicks>0) std::cout<<"Maximum simulation time reached. Simulation run aborted"<<std::endl;
    else {
      std::cout<<"A village (id: "<<status<<")was dropped after "<<(ticksBeforeFail+1)<<" ticks ("<<(ticksBeforeFail/60./60./20.)<<" h). Simulation finished successfully."<<std::endl;
      
    }
    tree->Fill();
  }
  //tree->Write();
  outfile->Write();
  outfile->Close();
  return 0;
}
