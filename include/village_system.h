#ifndef __village_system__
#define __village_system__
#include "TRandom3.h"
#include <stdlib.h>
#include <time.h>
#include "villager.h"
#include "configReader.h"
#include <list>

class village_system {

protected:
  std::vector<villager> vVillagers;
  std::list<int> queue;
  std::vector<int> villageTimer;
  std::vector<bool> vRelevance;
public:
  village_system(int numberOfVillagers );
  village_system(configReader* config);
  void print();
  int tick();
};
#endif
