#ifndef __CONFIGREADER__
#define __CONFIGREADER__
#include <stdlib.h>
#include <time.h>
#include "TEnv.h"
#include "TString.h"

class configReader {

protected:
  TEnv env;
  
public:
  configReader(TString& filename);
  int getNDetectors();
  int getNSpectators();
  int getNPopulation();
  int getDetectorMultiplicity();
  int getSpectatorMultiplicity();
  int getPopulationMultiplicity();
  int getNRuns();
  int getMaxTime();
  TString getOutputFileName();
  TString getTreeName(const TString& fallBack = "");
  
  //inline int getDelay() {return delay;};
  
  
  
};
#endif
