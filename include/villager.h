#ifndef __villager__
#define __villager__
#include "TRandom3.h"
#include <stdlib.h>
#include <time.h>

class villager {

protected:
  int id;
  int delay;
  TRandom3 rnd;//initialize a reasonably good prng with some lower quality prng
  
public:
  villager(int id);
  int tick();
  inline int getDelay() {return delay;};
  int getId();
  void setId(int id);
  
  
  
};
#endif
