CC=g++

NCORES=$(shell grep -ic ^processor /proc/cpuinfo)

LIBPATH=$(shell for line in $$(g++ --print-search-dirs | grep libraries | sed -e 's/:[ =]*/ /g'); do if [ -d $$line ]; then cd $$line; echo "-Wl,-rpath="$$(pwd); fi; done | sort -u)

MAIN=village_sim

ALL=$(shell hash $(ROOTCONFIG) 2>/dev/null; if [ $$? -eq 0 ]; then echo $(MAIN); else echo $(WARN); fi)

CXXFLAGS=-Iinclude -D__USE_XOPEN2K8 $(shell root-config --cflags) -Wall -fPIC 
LDFLAGS=-Llib $(shell root-config --libs)  -Wl,-rpath=lib -Wl,-rpath=$(shell root-config --libdir) $(LIBPATH)


all: $(ALL) $(INFOMSG)

parallel: all
	@echo -e "\n\n\e[00;31m\033[1m-= NOTIFICATION: You have compiled in parallel mode - this is EXPERIMENTAL and might cause severe crashes and/or harm your files =-\e[00m"

rootwarn:
	@echo "cannot build: root not found!"
obj/%.o: src/%.cxx
	g++ -c $^ $(CXXFLAGS) $(LDFLAGS)
	mv *.o obj/
	
%: %.cxx obj/*.o
	g++ -o $@ $^ $(CXXFLAGS) $(LDFLAGS)

clean:
	rm -f $(MAIN) obj/*.o 
