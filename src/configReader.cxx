#include <iostream>
#include <stdlib.h>
#include "configReader.h"
 
using namespace std;


configReader::configReader(TString& filename) {
  if (env.ReadFile(filename, kEnvLocal)<0) {
    std::cout<<"[ERROR] Failed to load configuration from file: \""<<filename<<"\" !";
  }
  std::cout<<"Loaded configuration:"<<std::endl;
  env.Print();
  
}


int configReader::getNDetectors() {
  return env.GetValue("village_sim.relevantVillagers.count",100);
}

int configReader::getNSpectators() {
  return env.GetValue("village_sim.irrelevantVillagers.count",0);
}

int configReader::getNPopulation() {
  return env.GetValue("village_sim.populationVillagers.count",0);
}

int configReader::getDetectorMultiplicity() {
  return env.GetValue("village_sim.relevantVillagers.weight",1); 
}

int configReader::getSpectatorMultiplicity() {
  return env.GetValue("village_sim.irrelevantVillagers.weight",21);
}

int configReader::getPopulationMultiplicity() {
  return env.GetValue("village_sim.populationVillagers.weight",21);
}


int configReader::getNRuns() {
  return env.GetValue("village_sim.nRuns",1000);
}

int configReader::getMaxTime() {
  return env.GetValue("village_sim.maxTime",1000);
}

TString configReader::getOutputFileName() {
  TString of = env.GetValue("village_sim.outFileName","");
  std::cout<<"Output File Name:"<<of<<std::endl;
  while (of.Length()<1) {
    //if (of.Length()>0) break;
    std::string helperString;  
    std::cout<<"Enter a file name to save results to: ";
    getline(std::cin,helperString);
    of = helperString;
  }
  if (!of.EndsWith(".root",TString::kIgnoreCase)) of.Append(".root");
  return of;
}

TString configReader::getTreeName(const TString& fallBack) {
  return TString(env.GetValue("village_sim.treeName",fallBack));
}


