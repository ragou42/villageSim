#include <iostream>
#include <stdlib.h>
#include <time.h>
#include "village_system.h"
#include "configReader.h"
 
using namespace std;


village_system::village_system(int numberOfVillagers) {

  for(int i=0;i<numberOfVillagers;i++) { //create villagers and store them in the corresponding vector
    vVillagers.push_back(villager(i));
    villageTimer.push_back(0);
    vRelevance.push_back(true);
  }
}

village_system::village_system(configReader* config) {
  int id = 0;
  for(int i=0;i<config->getNDetectors(); ++i) {
    for (int j=0;j<config->getDetectorMultiplicity(); ++j) {
      vVillagers.push_back(villager(id));
    }
    villageTimer.push_back(0);
    vRelevance.push_back(true);
    id++;
  }
  if (config->getNSpectators()>0) {
    for(int i=0;i<config->getNSpectators(); ++i) {
      for (int j=0;j<config->getSpectatorMultiplicity(); ++j) {
        vVillagers.push_back(villager(id));
      }
      villageTimer.push_back(0);
      vRelevance.push_back(false);
      id++;
    }
  }
  if (config->getNPopulation()>0) {
    for(int i=0;i<config->getNPopulation(); ++i) {
      for (int j=0;j<config->getPopulationMultiplicity(); ++j) {
        vVillagers.push_back(villager(id));
      }
      villageTimer.push_back(0);
      vRelevance.push_back(false);
      id++;
    }
  }
  std::cout<<"Village system initialized with "<<vVillagers.size()<<" villagers ("<<config->getNDetectors()<<" x "<<config->getDetectorMultiplicity()<<" detectors, "<<config->getNSpectators()<<" x "<<config->getSpectatorMultiplicity() <<" spectators, "<<config->getNPopulation()<<" x "<<config->getPopulationMultiplicity() <<" population villagers)"<<std::endl;
}

void village_system::print() {
  std::cout<<"Entries in vVillagers: "<<vVillagers.size()<<std::endl;
  std::cout<<"Entries in vRelevance: "<<vRelevance.size()<<std::endl;
  std::cout<<"Entries in villageTimer: "<<villageTimer.size()<<std::endl;
  for (uint i=0;i<vVillagers.size();i++) {
  std::cout<<"  index: "<<i<<", id: "<<vVillagers.at(i).getId()<<", relevance: "<< (vRelevance.at(vVillagers.at(i).getId()) ? "true" : "false") <<std::endl;
  }

  
}
int village_system::tick() {
  for (uint i=0;i<villageTimer.size();i++) {
    if (vRelevance.at(i)) villageTimer.at(i)++;
    if (villageTimer.at(i) >1200) return i;
    
  }
  int id;
  for (uint i=0;i<vVillagers.size();i++) {
    id = vVillagers.at(i).tick();
    if (id>-1 && this->queue.size()<65) {
      bool exists = false;
      for (std::list<int>::iterator it=this->queue.begin(); it != this->queue.end(); ++it) {
        if (! ((*it) == id)) continue;
        exists = true;
        break;
      }
      if (!exists) this->queue.push_back(id);
    }
  }
  
  if (this->queue.size()>0) {
    villageTimer.at(this->queue.front()) = 0;
    this->queue.pop_front();
  }
  return -1;  
}



