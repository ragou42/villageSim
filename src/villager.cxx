#include "villager.h"
#include <iostream>
#include <stdlib.h>
#include <sys/time.h>
using namespace std;


villager::villager(int id) {
  this->id = id;
  struct timeval tp;
  gettimeofday(&tp,NULL);
  rnd.SetSeed(tp.tv_sec * 1000 + tp.tv_usec);
  delay = 0;
}

int villager::tick() {
  if (delay<1) {
    delay = 70 + (int)(rnd.Rndm()*51); //51 since we cast to int = round down
    return this->id;
  }
  delay--;
  return -1;  
}

int villager::getId() {
  return this->id;
}

void villager::setId(int id) {
  this->id = id;
}


